#!/bin/bash
export PFM_PI_CONFIG=$HOME/develop/git/buildroot_pfm_utils/pi_config
alias cdmake="cd $HOME/develop/git/buildroot"
alias cdpicfg="cd $PFM_PI_CONFIG"
PATH=$PATH:$HOME/develop/git/buildroot/output/host/usr/bin
