#/bin/bash
PJT_ROOT=$PFM_PI_CONFIG
SD_BOOT=$PJT_ROOT/sd_boot
MNT_RFS=$PJT_ROOT/mnt_rfs
RFS_OUTPUT=$PJT_ROOT/RFS

mkdir -p $SD_BOOT
mkdir -p $MNT_RFS
mkdir -p $RFS_OUTPUT

cp ./output/images/zImage $SD_BOOT
cp ./output/images/rootfs.cpio $SD_BOOT
cp -R ./output/images/rpi-firmware/* $SD_BOOT
#cp ./output/images/*.txt $SD_BOOT

#overwrite cmdline created
#SIZE_RFS=`stat -c %s ./output/images/rootfs.cpio`
#sed -e "s/__SIZE__/$SIZE_RFS/g" $PJT_ROOT/cmdline.txt.template > $SD_BOOT/cmdline.txt

#serial port
mkdir -p $RFS_OUTPUT/etc
echo "ttyAMA0::respawn:/sbin/getty -L ttyAMA0 115200 vt100 # GENERIC_SERIAL" > $RFS_OUTPUT/etc/inittab

