dd if=/dev/zero  of=system.img bs=4M count=200
#
sudo losetup /dev/loop0 system.img
#
sudo parted -s /dev/loop0 mklabel msdos
#32 cylinders starting from 0
sudo parted -s /dev/loop0 unit cyl mkpart primary fat32 -- 0 32
sudo parted -s /dev/loop0 set 1 boot on
#use the remaining space - 4 cylinders. Start from cylinder 32
sudo parted -s /dev/loop0 unit cyl mkpart primary ext3 -- 32 -4
sudo mkfs.vfat -I -s 32 -n Boot /dev/loop0p1
sudo mkfs.ext3 -L RFS /dev/loop0p2
#
sudo mkdir /mnt/mnt_boot
sudo mkdir /mnt/rfs
#
sudo mount /dev/loop0p1 /mnt/mnt_boot
#
sudo cp -R $PFM_PI_CONFIG/sd_boot/* /mnt/mnt_boot
#
sudo dd if=~/develop/git/buildroot/output/images/rootfs.ext3 of=/dev/loop0p2
sudo mount /dev/loop0p2 /mnt/rfs
#sudo cp $PFM_PI_CONFIG/interfaces /mnt/rfs/etc/network/
#sudo echo -ne "\n#terminal in tty\ntty1::respawn:/sbin/getty tty1 38400" >> /mnt/rfs/etc/inittab
sudo cp $PFM_PI_CONFIG/RFS/etc/version.buildroot /mnt/rfs/etc/
#
sudo umount /mnt/rfs
sudo umount /mnt/mnt_boot/
sudo losetup --detach /dev/loop0
#
sudo rmdir /mnt/mnt_boot
sudo rmdir /mnt/rfs
#
#sudo dd  bs=4M if=system.img of=/dev/mmcblk0
#sudo mount -o loop,offset=263192576 system.img /mnt/foobar/
#
